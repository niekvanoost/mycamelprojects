fileToEmail
======================================================

Sends an email for every file found in a folder. Body of the email will be the file content. 

Based on: Camel in Action Chapter 1 - File Copy Example


Running
-----------

To run the example:

    mvn compile exec:java -Dexec.mainClass=camelinaction.FileToEmail
    

